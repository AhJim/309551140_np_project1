#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <vector>
using namespace std;
struct Pipe
{
	int fd[2];
	int count;
	Pipe(){
        pipe(fd);
    }
};
typedef const char* String;
void input(),number(vector<char *>,int pipeto,bool merr),exe(string command,int pipeto,bool merr);
vector<char *> stringToChar(vector<string> s);
vector<Pipe> pipeVector;
vector<pid_t> Pid;

int main()
{
	setenv("PATH","bin:.",true);
	input();
    return 0;
}

void input(){
	string in;
	cout << "% ";
	getline(cin,in);
	int last = in.find(" ", 0),start = 0;
	
	if (in == ""){
	}else if (in == "exit"){
		exit(0); 
	}else if (in.substr(0,last) == "printenv"){
        cout << getenv(in.substr(last+1).c_str())<<endl;
	}else if(in.substr(0,last) == "setenv"){
		int i = in.find(" ", last+1);
		setenv(in.substr(last+1,i-last-1).c_str(),in.substr(i+1).c_str(), true);
	}else{
		bool flag = true,merr = false;
		int pipeto,clast;
		string line;
		while(flag){
			if (in.find("|", start) != -1){
				if (in.find("!", start) != -1){
					if (in.find("|", start) < in.find("!", start)){
						last = in.find("|", start);
						merr = false;
					}else{
						last = in.find("!", start);
						merr = true;
					}
				}else{
					last = in.find("|", start);
					merr = false;
				}
			}else{
				last = in.find("!", start);
				merr = true;
			}
			pipeto = 0;
			if (last > 0){
				clast = in.find(" ", last+1);
				if (clast > 0){
					if (clast - last == 1){
						pipeto = 1;
					}else{
						pipeto = stoi(in.substr(last+1,clast - last-1));
					}
					line = in.substr(start,last-start-1);
					start = clast+1;
				}else{
					flag = false;
					line = in.substr(start,last-start-1);
					start = last+1;
				}	
			}else{
				flag = false;
				line = in.substr(start,last-start);
			}
			exe(line,pipeto,merr);
		}
	}
	input();
} 

void exe(string command,int pipeto,bool merr){
	vector<string> commandVector;
	int last = 0,start = 0,existpipe = -1;
	while(last != -1){
		last = command.find(" ", start);
		if (command.substr(start,last-start) != ""){
			commandVector.push_back(command.substr(start,last-start));
			start = last+1;
		}
	}
	for (int i = 0 ; i < pipeVector.size() ; i++){
		pipeVector[i].count--;
		if (pipeVector[i].count == pipeto)
			existpipe = i;
	}
	Pipe p;
	p.count = pipeto;
	pid_t pid;
	while ((pid = fork()) < 0) {
		int status;
		waitpid(-1, &status, 0);
	}
	if(pid == 0) {
		for (int i = 0 ; i < pipeVector.size() ; i++){
			if(pipeVector[i].count == 0){
				dup2(pipeVector[i].fd[0],STDIN_FILENO);
				close(pipeVector[i].fd[0]);
				close(pipeVector[i].fd[1]);
			}
		}
		if (pipeto > 0){
			if (existpipe == -1){
				if (merr)
					dup2(p.fd[1], STDERR_FILENO);
				dup2(p.fd[1], STDOUT_FILENO);
				close(p.fd[0]);
				close(p.fd[1]);
			}else{
				if (merr)
					dup2(pipeVector[existpipe].fd[1], STDERR_FILENO);
				dup2(pipeVector[existpipe].fd[1], STDOUT_FILENO);
			}
		}
		vector<char *> command_char = stringToChar(commandVector);
		command_char.push_back(NULL);
		char **Command = &command_char[0];
		execvp(Command[0], Command) == -1);
		exit(0);
	}else {
		for (int i = 0 ; i < pipeVector.size() ; i++){
			if(pipeVector[i].count == 0){
				close(pipeVector[i].fd[0]);
				close(pipeVector[i].fd[1]);
				pipeVector.erase(pipeVector.begin()+i);	
				i--;
			}
		}
		if (existpipe == -1 && pipeto > 0)
			pipeVector.push_back(p);
		Pid.push_back(pid);
		if (pipeto == 0){
			for (pid_t i = 0 ; i < Pid.size() ; i++){
				int status;
				waitpid(Pid[i], &status, 0);
			}
			Pid.clear();
		}
	}
}

vector<char *> stringToChar(vector<string> s){
    vector<char *> c;
	for(int i = 0; i < s.size(); ++i){
        char *str;
        str = new char[s[i].size() + 1];
        memcpy(str, s[i].c_str(), s[i].size() + 1);
        c.push_back(str);
    }
    return c;
}